﻿using ContosoUniversity.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContosoUniversity.Services
{
    public class UserService
    {
        private string currentUserName { get; set; }
        private string currentPassword { get; set; }
        private SchoolContext db = new SchoolContext();

        public UserService()
        {
            this.currentUserName = "null";
            this.currentPassword = "null";
        }

        public UserService(string currentUserName, string currentPassword)
        {
            this.currentUserName = currentUserName;
            this.currentPassword = currentPassword;
        }
        
        public string GetCurrentUserName()
        {
            var result = db.Users.Where(u => u.Chucvu == "Login").First();
            this.currentUserName = result.UserName;

            return currentUserName;
        }

        public void UserServiceLogOut()
        {
            this.currentUserName = "logout";
            this.currentPassword = "logout";
        }
    }
}