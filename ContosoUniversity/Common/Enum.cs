﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContosoUniversity.Common
{
    public class Enum
    {

    }
    public enum Calamviec: int
    {
        //[Display(Name = "Ca 1 (7h30 - 16h15)")]
        [Description("Ca 1 (7h30 - 16h15)")]
        Ca1 = 1,
        [Description("Ca 2 (16h30 - 8h00)")]
        Ca2 = 2,
        //[Display(Name = "Ca 3")]
        //Ca3 = 3
    }
    public enum TrangThaiXuLy : int
    {
        [Description("Chưa xử lý")]
        CXL = 1,
        [Description("Đang sửa chữa")]
        DSC = 2,
        [Description("Đã hoàn thành")]
        DHT = 3,
        [Description("Khác")]
        KH = 4
    }
    
}