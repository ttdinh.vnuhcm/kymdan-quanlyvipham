﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContosoUniversity.DTO
{
    public class LinkHanhViViPhamDTO
    {
        public int LinkHanhViViPhamID { get; set; }
        public string Ten { get; set; }
        public int? LuotViPhamID { get; set; }
    }
}