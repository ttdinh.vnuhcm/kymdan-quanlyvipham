﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContosoUniversity.DTO
{
    public class LuotViPhamDTO
    {
        public int LuotViPhamID { get; set; }
        [Display(Name = "Nội dung vi phạm")]
        public string NoiDungViPham { get; set; }
        [Display(Name = "Trạng thái")]
        public string TrangThaiXuLy { get; set; }
        [Display(Name = "Loại vi phạm")]
        public string LoaiViPham { get; set; }
        public int? ChuTheID { get; set; }
        [Display(Name = "Tên file")]
        public HttpPostedFileBase[] TenFiles { get; set; }

        //Thông tin chủ thể
        [Display(Name = "Tên chủ thể")]
        public string TenChuThe { get; set; }
        [Display(Name = "Địa chỉ")]
        public string DiaChi { get; set; }

        [Display(Name = "Số điện thoại")]
        public string SDT { get; set; }

        [Display(Name = "Thông tin website")]
        public string ThongTinWebsite { get; set; }

        [Display(Name = "Thông tin facebook")]
        public string ThongTinFacebook { get; set; }

        [Display(Name = "Thông tin zalo")]
        public string ThongTinZalo { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Người đại diện pháp luật")]
        public string NguoiDaiDienPhapLuat { get; set; }

        [Display(Name = "Giấy phép kinh doanh")]
        public string GiayPhepKinhDoanh { get; set; }

        [Display(Name = "Link dẫn đến hành vi vi phạm")]
        public string LinkHanhViViPham { get; set; }

    }
    public class LuotViPhamSearch
    {
        [Display(Name = "Nội dung vi phạm")]
        public string NoiDungViPham { get; set; }
        [Display(Name = "Trạng thái xử lý")]
        public string TrangThaiXuLy { get; set; }
        
        public string LoaiViPham { get; set; }

    }
}