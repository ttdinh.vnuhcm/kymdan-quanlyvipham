﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContosoUniversity.DTO
{
    public class ChuTheDTO
    {
        public int ChuTheID { get; set; }
        public string TenChuThe { get; set; }
        public string DiaChi { get; set; }
        public string SDT { get; set; }
        public string Email { get; set; }
        public string ThongTinWebSite { get; set; }
        public string ThongTinFacebook { get; set; }
        public string ThongTinZalo { get; set; }
        public string GiayPhepKinhDoanh { get; set; }
        public string NguoiDaiDienPhapLuat { get; set; }
        public bool? IsKetThucTheoDoi { get; set; }
    }
}