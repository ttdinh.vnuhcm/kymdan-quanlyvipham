﻿/**/using ContosoUniversity.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ContosoUniversity.DAL
{
    public class SchoolContext : DbContext
    {        

        public DbSet<OperationLog> OperationLogs { get; set; }   

          

        
        public DbSet<User> Users { get; set; }

        public DbSet<ActionLog> ActionLogs { get; set; }
        public DbSet<UserAccount> UserAccounts { get; set; }

        public DbSet<ChuThe> ChuThes { get; set; }
        public DbSet<FileDinhKem> FileDinhKems { get; set; }
        public DbSet<LichSuLienHe> LichSuLienHes { get; set; }
        public DbSet<LinkHanhViViPham> LinkHanhViViPhams { get; set; }
        public DbSet<LuotViPham> LuotViPhams { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

        }
    }
}