﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;



namespace ContosoUniversity.Models
{
    public class OperationLog : BaseModel
    {
        public int OperationLogID { get; set; }
        public string OperationCode { get; set; }
        public string Action { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ReferenceTable { get; set; }
    }
}