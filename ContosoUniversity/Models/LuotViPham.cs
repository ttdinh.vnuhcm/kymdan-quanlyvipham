﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContosoUniversity.Models
{
    public class LuotViPham : BaseModel
    {
        public int LuotViPhamID { get; set; }
        public string NoiDungViPham { get; set; }
        public string TrangThaiXuLy { get; set; }
        public string LoaiViPham { get; set; }
        public int? ChuTheID { get; set; }

    }
}