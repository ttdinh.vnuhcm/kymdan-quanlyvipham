﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContosoUniversity.Models
{
    public class ChuThe : BaseModel
    {
        public int ChuTheID { get; set; }

        [Display(Name = "Tên chủ thể")]
        public string TenChuThe { get; set; }
        [Display(Name = "Địa chỉ")]
        public string DiaChi { get; set; }
        [Display(Name = "SĐT")]
        public string SDT { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Thông tin Website")]
        public string ThongTinWebSite { get; set; }
        [Display(Name = "Thông tin Facebook")]
        public string ThongTinFacebook { get; set; }
        [Display(Name = "Thông tin Zalo")]
        public string ThongTinZalo { get; set; }
        [Display(Name = "Giấy phép kinh doanh")]
        public string GiayPhepKinhDoanh { get; set; }
        [Display(Name = "Người đại diện pháp luật")]
        public string NguoiDaiDienPhapLuat { get; set; }
        [Display(Name = "Đã kết thúc theo dõi")]
        public bool? IsKetThucTheoDoi { get; set; }
    }
}