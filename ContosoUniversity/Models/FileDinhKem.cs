﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContosoUniversity.Models
{
    public class FileDinhKem : BaseModel
    {
        public int FileDinhKemID { get; set; }
        public string TenFile { get; set; }
        public int? LichSuLienHeID { get; set; }
        public int? LuotViPhamID { get; set; }
    }
}