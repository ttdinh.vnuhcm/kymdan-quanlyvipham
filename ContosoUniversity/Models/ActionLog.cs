﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContosoUniversity.Models
{
    public class ActionLog : BaseModel
    {
        public int ActionLogId { get; set; }
        public string OperationCode { get; set; }
        public string Action { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ReferenceTable { get; set; }
    }
}