﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContosoUniversity.Models
{
    public class LichSuLienHe : BaseModel
    {
        public int LichSuLienHeID { get; set; }
        [Display(Name = "Ngày liên hệ")]
        public DateTime? NgayLienHe { get; set; }
        [Display(Name = "Ghi chú")]
        public string GhiChu { get; set; }
        public int? LuotViPhamID { get; set; }
    }
}