﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContosoUniversity.Models
{
    public class User : BaseModel
    {
        public int UserID { get; set; }
        [Display(Name = "Tên tài khoản")]
        public string UserName { get; set; }
        [Display(Name = "Mật khẩu")]
        public string HashPassword { get; set; }
        public string Email { get; set; }
        public string Chucvu { get; set; }

    }
}