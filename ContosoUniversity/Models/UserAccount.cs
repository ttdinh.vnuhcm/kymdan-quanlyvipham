﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContosoUniversity.Models
{
    public class UserAccount : BaseModel
    {
        public int UserAccountID { get; set; }
        public string UserName { get; set; }
        
        public string HashPassword { get; set; }
        public string Email { get; set; }
        public string Chucvu { get; set; }
    }
}