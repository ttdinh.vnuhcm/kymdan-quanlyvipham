﻿using ContosoUniversity.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContosoUniversity.Controllers
{
    public class LuotViPhamController : Controller
    {
        // GET: LuotViPham
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DanhSachLuotViPham()
        {
            return View();
        }
        public ActionResult PopupAddViPham()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult SaveChuTheVaLuotViPham(LuotViPhamDTO luotvipham)
        {
            foreach (HttpPostedFileBase file in luotvipham.TenFiles)
            {
                //Checking file is available to save.  
                if (file != null)
                {
                    string _FileName = Path.GetFileName(file.FileName);
                    string _path = Path.Combine(Server.MapPath("~/Content/Upload/Images"), _FileName);

                    file.SaveAs(_path);
                    string url = HttpContext.Request.Url.Authority.ToString();
                    string hoso = url + "/Content/Upload/Images/" + _FileName;
                }

            }
            
            return View();
        }
    }
}