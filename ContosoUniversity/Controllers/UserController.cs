﻿using ContosoUniversity.DAL;
using ContosoUniversity.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ContosoUniversity.Controllers
{
    public class UserController : Controller
    {
        private SchoolContext db = new SchoolContext();
        // GET: User
        public ActionResult Index(int? page = 1, int? pageSize = 20)
        {
            /*if (Session["User"] != null && Session["role"] != null)*/
            {
                /*if (Session["role"].ToString() == "Admin")*/
                {
                    int pageIndex = 1;
                    pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

                    int defaSize = (pageSize ?? 20);

                    ViewBag.psize = defaSize;

                    //Dropdownlist code for PageSize selection  
                    //In View Attach this  
                    ViewBag.PageSize = new List<SelectListItem>()
                {
                    new SelectListItem() { Value="5", Text= "5" },
                    new SelectListItem() { Value="10", Text= "10" },
                    new SelectListItem() { Value="25", Text= "25" },
                    new SelectListItem() { Value="50", Text= "50" },
                 };

                    var _users = db.Users.Where(x => x.Chucvu != "Admin").OrderByDescending(x => x.UserID);

                    var res = _users.ToPagedList(pageIndex, defaSize);
                    return View(res);
                }                
            }

            /*return RedirectToAction("Index", "Home");*/
        }



        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(User user)
        {
            User previoususer = db.Users.Where(x => x.UserName == user.UserName).FirstOrDefault();
            if (previoususer != null)
                return View(user);

            User usermodel = new User();
            usermodel.UserName = user.UserName;
            usermodel.HashPassword = user.HashPassword;
            usermodel.Email = user.Email;
            usermodel.Chucvu = user.Chucvu;
            usermodel.IdentityId = user.UserID;
            usermodel.CreateBy = "abc";
            usermodel.CreateDate = DateTime.Now;
            usermodel.UpdateBy = "abc";
            usermodel.UpdateDate = DateTime.Now;



            if (ModelState.IsValid)
            {
                db.Users.Add(usermodel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: User/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            User user = db.Users.Find(id);

            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
            
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(int? id, User user)
        {
            var usermodel = db.Users.Find(user.UserID);

            usermodel.UserName = user.UserName;
            usermodel.HashPassword = user.HashPassword;
            usermodel.Email = user.Email;
            usermodel.Chucvu = user.Chucvu;
            usermodel.IdentityId = user.UserID;
            usermodel.CreateBy = "abc";
            usermodel.CreateDate = DateTime.Now;
            usermodel.UpdateBy = "abc";
            usermodel.UpdateDate = DateTime.Now;
            
            if (ModelState.IsValid)
            {
                db.Entry(usermodel).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }



            return View(user);
        }

        // GET: User/Delete/5
        public ActionResult Delete(int? id)
        {
            User usermodel = db.Users.Find(id);
            db.Users.Remove(usermodel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }

}
