﻿using ContosoUniversity.DAL;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContosoUniversity.Controllers
{
    public class QuanlyLichSuLienHeController : Controller
    {
        private SchoolContext db = new SchoolContext();

        // GET: QuanlyLichSuLienHe
        public ActionResult Index(int? page = 1, int? pageSize = 10)
        {
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            int defaSize = (pageSize ?? 10);

            ViewBag.psize = defaSize;

            //Dropdownlist code for PageSize selection  
            //In View Attach this  
            ViewBag.PageSize = new List<SelectListItem>()
                {                    
                    new SelectListItem() { Value="10", Text= "10" },
                    new SelectListItem() { Value="25", Text= "25" },
                    new SelectListItem() { Value="50", Text= "50" },
                 };



            ViewModels.QuanlyLichsuLienheViewModel res = new ViewModels.QuanlyLichsuLienheViewModel();
            var _quanlylichsulienhe = db.LichSuLienHes.Where(x => x.IdentityId > 0);

            List<int> cntQuanlyLienhe = new List<int>();

            res.pagination = _quanlylichsulienhe.OrderByDescending(x => x.CreateDate).ToPagedList(pageIndex, defaSize);
            res.cntQuanlylienhe = cntQuanlyLienhe;

            ViewBag.cntQuanlyme = cntQuanlyLienhe;

            ViewBag.total = res.pagination.TotalItemCount;
            return View(res);            
        }

        // GET: QuanlyLichSuLienHe/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: QuanlyLichSuLienHe/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: QuanlyLichSuLienHe/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: QuanlyLichSuLienHe/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: QuanlyLichSuLienHe/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: QuanlyLichSuLienHe/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: QuanlyLichSuLienHe/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
