﻿using ContosoUniversity.DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ContosoUniversity.Models;

namespace ContosoUniversity.Controllers
{
    public class UploadFileController : Controller
    {
        private SchoolContext db = new SchoolContext();

        // GET: Upload
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult UploadFile(int? id)
        {
/*            if (Session["User"] == null)
                return RedirectToAction("LoginIndex", "Login");
            if (id == null)
                return RedirectToAction("Index", "ThongtinSuCo");*/                

            ViewBag.id = id;

            return View();
        }

        [HttpPost]
        public ActionResult UploadFile(int? id, HttpPostedFileBase file)
        {
            if (Session["User"] == null)
                return RedirectToAction("LoginIndex", "Login");
            if (id != null)
            {

/*                ThongtinSuCo thongtinSuCo = new ThongtinSuCo();
                thongtinSuCo = db.ThongtinSuCos.Find(id);
*/
                try
                {
                    if (file.ContentLength > 0)
                    {
                        string _FileName = Path.GetFileName(file.FileName);
                        string _path = Path.Combine(Server.MapPath("~/Content/Upload/Images"), _FileName);
                        
                        file.SaveAs(_path);
                        string url = HttpContext.Request.Url.Authority.ToString();
                        string hoso = url + "/Content/Upload/Images/" + _FileName;

                        /*thongtinSuCo.HoSo = hoso;*/
                        /*db.ThongtinSuCos.Add(thongtinSuCo);*/
                        db.SaveChanges();
                    }
                    
                    

                    ViewBag.Message = "File Uploaded Successfully!!";
                    return View();
                }
                catch
                {
                    ViewBag.Message = "File upload failed!!";
                    return View();
                }
            }

            return View();
        }
    }

/*    public class UploadFileController : Controller
    {
        // GET: UploadFile
        public ActionResult Index()
        {
            return View();
        }
    }*/
}