﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using ContosoUniversity.Common;
using ContosoUniversity.DAL;
using ContosoUniversity.Models;
using PagedList;
using ContosoUniversity.Services;


namespace ContosoUniversity.Controllers
{
    public class QuanlyChuTheController : Controller
    {
        private SchoolContext db = new SchoolContext();

        // GET: QuanlyChuThe
        public ActionResult Index(int? page = 1, int? pageSize = 10)
        {
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            int defaSize = (pageSize ?? 10);

            ViewBag.psize = defaSize;

            //Dropdownlist code for PageSize selection  
            //In View Attach this  
            ViewBag.PageSize = new List<SelectListItem>()
                {
                    //new SelectListItem() { Value="5", Text= "5" },
                    new SelectListItem() { Value="10", Text= "10" },
                    new SelectListItem() { Value="25", Text= "25" },
                    new SelectListItem() { Value="50", Text= "50" },
                 };



            ViewModels.QuanlyChuTheViewModel res = new ViewModels.QuanlyChuTheViewModel();
            var _quanlychuthe = db.ChuThes.Where(x => x.IdentityId > 0);            

            List<int> cntQuanlychuThe = new List<int>();           

            res.pagination = _quanlychuthe.OrderByDescending(x => x.CreateDate).ToPagedList(pageIndex, defaSize);
            res.cntQuanlychuThe = cntQuanlychuThe;

            ViewBag.cntQuanlyme = cntQuanlychuThe;

            ViewBag.total = res.pagination.TotalItemCount;
            return View(res);            
        }

        // GET: QuanlyChuThe/Details/5
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            /*QuanlyMe quanlyMe = db.quanlyMes.Find(id);*/
            ChuThe quanlyChuthe = db.ChuThes.Find(id);

            if ( quanlyChuthe == null)
            {
                return HttpNotFound();
            }
            return View(quanlyChuthe);            
        }

        // GET: QuanlyChuThe/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: QuanlyChuThe/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: QuanlyChuThe/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: QuanlyChuThe/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: QuanlyChuThe/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var res = db.ChuThes.Find(id);

            return View(res);            
        }

        // POST: QuanlyChuThe/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
