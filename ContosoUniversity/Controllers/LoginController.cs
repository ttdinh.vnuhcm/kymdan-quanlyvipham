﻿using ContosoUniversity.DAL;
using ContosoUniversity.Models;
using ContosoUniversity.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContosoUniversity.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        private SchoolContext db = new SchoolContext();
        private UserService userService;

        [HttpGet]
        public ActionResult LoginIndex()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginIndex(User userLogin)
        {
            var listofresult = db.Users.Where(u => u.UserName == userLogin.UserName).Where(u => u.HashPassword == userLogin.HashPassword).ToList();
            if (listofresult.Count > 0)
            {                
                var result = listofresult[0];
                /*var result = db.Users.Where(u => u.UserName == userLogin.UserName).Where(u => u.HashPassword == userLogin.HashPassword).First();*/
                Session["User"] = result.UserName;
                Session["role"] = result.Chucvu;
                //result.Chucvu = "Login";
                //if (ModelState.IsValid)
                //    {
                //        db.Entry(result).State = System.Data.Entity.EntityState.Modified;
                //        db.SaveChanges();
                //}
                ViewBag.ValidateError = "";
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.ValidateError = "Sai tên tài khoản hoặc mật khẩu";
            }
            return View();
        }

        public ActionResult LogOut()
        {
            /*userService.UserServiceLogOut();*/
            /*var result = db.Users.Where(u => u.Chucvu == "Login").First();
            if (result != null)
            {
                result.Chucvu = "Logout";
                if (ModelState.IsValid)
                {
                    db.Entry(result).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }*/

            
            //Clear session
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();
            // Some More code

            return RedirectToAction("LoginIndex", "Login");

        }
    }
}