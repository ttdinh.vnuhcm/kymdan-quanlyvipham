﻿namespace ContosoUniversity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initdatabase : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.ChitietSuCo");
            DropTable("dbo.Nhatkyvanhanh");
            DropTable("dbo.ThongtinMe");
            DropTable("dbo.ThongtinSuCo");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ThongtinSuCo",
                c => new
                    {
                        ThongtinSuCoID = c.Int(nullable: false, identity: true),
                        ThongTinSuCoCode = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        CreateBy = c.String(),
                        UpdateDate = c.DateTime(nullable: false),
                        UpdateBy = c.String(),
                        LoaiSuCo = c.String(),
                        MoTaNganSuCo = c.String(),
                        NguoiMota = c.String(),
                        Cachkhacphuc = c.String(),
                        NguoiDeNghiKhacPhuc = c.String(),
                        Ketqua = c.String(),
                        NhatKyID = c.Int(),
                        Trangthaixuly = c.String(),
                        Quatrinhxuly = c.String(),
                        HoSo = c.String(),
                        LoaiSuCoThoigianvanhanh = c.Boolean(nullable: false),
                        LoaiSuCochisohoahoc = c.Boolean(nullable: false),
                        LoaSuCoHuHongThietBi = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ThongtinSuCoID);
            
            CreateTable(
                "dbo.ThongtinMe",
                c => new
                    {
                        ThongTinMeID = c.Int(nullable: false, identity: true),
                        ThongTinMeCode = c.String(),
                        CreateBy = c.String(),
                        CreateDate = c.DateTime(),
                        UpdateBy = c.String(),
                        UpdateDate = c.DateTime(),
                        Begom = c.String(),
                        CycloTron = c.String(),
                        Belang = c.String(),
                        BomHoaChat = c.String(),
                        BeDieuHoa = c.String(),
                        BeSBR1 = c.String(),
                        BeSBR2 = c.String(),
                        MayEpBun = c.String(),
                        identityIdMe = c.String(),
                        idMe = c.Int(nullable: false),
                        dayTimeFrom = c.DateTime(),
                        dayTimeTo = c.DateTime(),
                        userUpdateMe = c.String(),
                        timeUpdateMe = c.DateTime(),
                        begomPh = c.String(),
                        begomHoahocCod = c.String(),
                        begomHoahocTss = c.String(),
                        begomHoahocNt = c.String(),
                        begomHoahocP = c.String(),
                        begomTimeBom = c.DateTime(),
                        cyclotronTimeXaday = c.DateTime(),
                        belangTimeXaday = c.DateTime(),
                        bomhoachatNongdo = c.String(),
                        bomhoachatTimeBom = c.DateTime(),
                        bedieuhoaDo = c.String(),
                        bedieuhoaTimeBom = c.DateTime(),
                        bedieuhoaTimeThoikhi = c.DateTime(),
                        besbr1Ph = c.String(),
                        besbr1Do = c.String(),
                        besbr1HoahocCod = c.String(),
                        besbr1HoahocTss = c.String(),
                        besbr1HoahocNt = c.String(),
                        besbr1HoahocP = c.String(),
                        besbr1Vl = c.String(),
                        besbr1TimeLamday = c.DateTime(),
                        besbr1TimeXuly = c.DateTime(),
                        besbr1TimeLang = c.DateTime(),
                        besbr1TimeXa = c.DateTime(),
                        besbr1TimeCho = c.DateTime(),
                        besbr1TimeBombun = c.DateTime(),
                        besbr2Ph = c.String(),
                        besbr2Do = c.String(),
                        besbr2HoahocCod = c.String(),
                        besbr2HoahocTss = c.String(),
                        besbr2HoahocNt = c.String(),
                        besbr2HoahocP = c.String(),
                        besbr2Vl = c.String(),
                        besbr2TimeLamday = c.DateTime(),
                        besbr2TimeXuly = c.DateTime(),
                        besbr2TimeLang = c.DateTime(),
                        besbr2TimeXa = c.DateTime(),
                        besbr2TimeCho = c.DateTime(),
                        besbr2TimeBombun = c.DateTime(),
                        mayepbunTimeHoatdong = c.DateTime(),
                        mayepbunKhoiluongbunkho = c.String(),
                    })
                .PrimaryKey(t => t.ThongTinMeID);
            
            CreateTable(
                "dbo.Nhatkyvanhanh",
                c => new
                    {
                        NhatkyvanhanhID = c.Int(nullable: false, identity: true),
                        MotaNhatky = c.String(),
                        Thongtinsuco = c.String(),
                        Luuluongvao = c.Single(nullable: false),
                        Luuluongra = c.Single(nullable: false),
                        Chisotieuthudien = c.Single(nullable: false),
                        Khoiluongtieuthudien = c.Single(nullable: false),
                        polymervao = c.Single(nullable: false),
                        polymerra = c.Single(nullable: false),
                        phabotvao = c.Single(nullable: false),
                        phabotra = c.Single(nullable: false),
                        NguoiTruc = c.String(),
                        MeID = c.Int(),
                        NaOHvao = c.Single(nullable: false),
                        NaOHra = c.Single(nullable: false),
                        NPKvao = c.Single(nullable: false),
                        NPKra = c.Single(nullable: false),
                        Urevao = c.Single(nullable: false),
                        Urera = c.Single(nullable: false),
                        Matrivao = c.Single(nullable: false),
                        Matrira = c.Single(nullable: false),
                        KiemtraChungHeThong = c.Boolean(nullable: false),
                        GhiNhanSuCo = c.String(),
                        GhiChuCaSau = c.String(),
                        PolymerTon = c.Single(nullable: false),
                        PhaBotTon = c.Single(nullable: false),
                        NaOHTon = c.Single(nullable: false),
                        UreTon = c.Single(nullable: false),
                        MatriTon = c.Single(nullable: false),
                        Ghinhanbatthuong = c.String(),
                        IdentityId = c.Int(nullable: false),
                        CreateDate = c.DateTime(),
                        CreateBy = c.String(),
                        UpdateDate = c.DateTime(),
                        UpdateBy = c.String(),
                    })
                .PrimaryKey(t => t.NhatkyvanhanhID);
            
            CreateTable(
                "dbo.ChitietSuCo",
                c => new
                    {
                        ChitietSuCoId = c.Int(nullable: false, identity: true),
                        Ngaytao = c.String(),
                        Nguoitao = c.String(),
                        Loaighichu = c.String(),
                        Noidungghichu = c.String(),
                        Dinhkem = c.String(),
                        IdentityId = c.Int(nullable: false),
                        CreateDate = c.DateTime(),
                        CreateBy = c.String(),
                        UpdateDate = c.DateTime(),
                        UpdateBy = c.String(),
                    })
                .PrimaryKey(t => t.ChitietSuCoId);
            
        }
    }
}
