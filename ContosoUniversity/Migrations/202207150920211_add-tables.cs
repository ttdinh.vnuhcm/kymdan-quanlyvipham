﻿namespace ContosoUniversity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActionLog",
                c => new
                    {
                        ActionLogId = c.Int(nullable: false, identity: true),
                        OperationCode = c.String(),
                        Action = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ReferenceTable = c.String(),
                        IdentityId = c.Int(nullable: false),
                        CreateDate = c.DateTime(),
                        CreateBy = c.String(),
                        UpdateDate = c.DateTime(),
                        UpdateBy = c.String(),
                    })
                .PrimaryKey(t => t.ActionLogId);
            
            CreateTable(
                "dbo.UserAccount",
                c => new
                    {
                        UserAccountID = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        HashPassword = c.String(),
                        Email = c.String(),
                        Chucvu = c.String(),
                        IdentityId = c.Int(nullable: false),
                        CreateDate = c.DateTime(),
                        CreateBy = c.String(),
                        UpdateDate = c.DateTime(),
                        UpdateBy = c.String(),
                    })
                .PrimaryKey(t => t.UserAccountID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserAccount");
            DropTable("dbo.ActionLog");
        }
    }
}
