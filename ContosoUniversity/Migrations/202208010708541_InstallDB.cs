﻿namespace ContosoUniversity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InstallDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChuThe",
                c => new
                    {
                        ChuTheID = c.Int(nullable: false, identity: true),
                        TenChuThe = c.String(),
                        DiaChi = c.String(),
                        SDT = c.String(),
                        Email = c.String(),
                        ThongTinWebSite = c.String(),
                        ThongTinFacebook = c.String(),
                        ThongTinZalo = c.String(),
                        GiayPhepKinhDoanh = c.String(),
                        NguoiDaiDienPhapLuat = c.String(),
                        IsKetThucTheoDoi = c.Boolean(),
                        IdentityId = c.Int(nullable: false),
                        CreateDate = c.DateTime(),
                        CreateBy = c.String(),
                        UpdateDate = c.DateTime(),
                        UpdateBy = c.String(),
                    })
                .PrimaryKey(t => t.ChuTheID);
            
            CreateTable(
                "dbo.FileDinhKem",
                c => new
                    {
                        FileDinhKemID = c.Int(nullable: false, identity: true),
                        TenFile = c.String(),
                        LichSuLienHeID = c.Int(),
                        LuotViPhamID = c.Int(),
                        IdentityId = c.Int(nullable: false),
                        CreateDate = c.DateTime(),
                        CreateBy = c.String(),
                        UpdateDate = c.DateTime(),
                        UpdateBy = c.String(),
                    })
                .PrimaryKey(t => t.FileDinhKemID);
            
            CreateTable(
                "dbo.LichSuLienHe",
                c => new
                    {
                        LichSuLienHeID = c.Int(nullable: false, identity: true),
                        NgayLienHe = c.DateTime(),
                        GhiChu = c.String(),
                        LuotViPhamID = c.Int(),
                        IdentityId = c.Int(nullable: false),
                        CreateDate = c.DateTime(),
                        CreateBy = c.String(),
                        UpdateDate = c.DateTime(),
                        UpdateBy = c.String(),
                    })
                .PrimaryKey(t => t.LichSuLienHeID);
            
            CreateTable(
                "dbo.LinkHanhViViPham",
                c => new
                    {
                        LinkHanhViViPhamID = c.Int(nullable: false, identity: true),
                        Ten = c.String(),
                        LuotViPhamID = c.Int(),
                        IdentityId = c.Int(nullable: false),
                        CreateDate = c.DateTime(),
                        CreateBy = c.String(),
                        UpdateDate = c.DateTime(),
                        UpdateBy = c.String(),
                    })
                .PrimaryKey(t => t.LinkHanhViViPhamID);
            
            CreateTable(
                "dbo.LuotViPham",
                c => new
                    {
                        LuotViPhamID = c.Int(nullable: false, identity: true),
                        NoiDungViPham = c.String(),
                        TrangThaiXuLy = c.String(),
                        LoaiViPham = c.String(),
                        ChuTheID = c.Int(),
                        IdentityId = c.Int(nullable: false),
                        CreateDate = c.DateTime(),
                        CreateBy = c.String(),
                        UpdateDate = c.DateTime(),
                        UpdateBy = c.String(),
                    })
                .PrimaryKey(t => t.LuotViPhamID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LuotViPham");
            DropTable("dbo.LinkHanhViViPham");
            DropTable("dbo.LichSuLienHe");
            DropTable("dbo.FileDinhKem");
            DropTable("dbo.ChuThe");
        }
    }
}
