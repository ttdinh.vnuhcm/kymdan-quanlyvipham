﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContosoUniversity.ViewModels
{
    public class OperationLogViewModel
    {
        public PagedList.IPagedList<ContosoUniversity.Models.OperationLog> pagination { get; set; }
        public DateTime? fromDate { get; set; }
        public DateTime? toDate { get; set; }
    }
}