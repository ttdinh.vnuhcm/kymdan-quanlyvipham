﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContosoUniversity.ViewModels
{
    public class QuanlyLichsuLienheViewModel
    {
        public PagedList.IPagedList<ContosoUniversity.Models.LichSuLienHe> pagination { get; set; }
        public List<int> cntQuanlylienhe { get; set; }

    }
}